var fontsize;
var index = 35;


function init(){
  var slider = new Slider('#ex1', {
	formatter: function(value) {
    fontsize = value;
		return 'Current value: ' + value;
	}
  });
  slider.on('slideStop', function(){
    article = document.getElementsByTagName("article")[0];
    setFontSize(article);
  });

}


function getStyle(el,styleProp) {
  var camelize = function (str) {
    return str.replace(/\-(\w)/g, function(str, letter){
      return letter.toUpperCase();
    });
  };

  if (el.currentStyle) {
    return el.currentStyle[camelize(styleProp)];
  } else if (document.defaultView && document.defaultView.getComputedStyle) {
    return document.defaultView.getComputedStyle(el,null)
                               .getPropertyValue(styleProp);
  } else {
    return el.style[camelize(styleProp)]; 
  }
}

function setFontSize(object){
  console.log(fontsize);
  // article = document.getElementsByTagName("article")[0];
  mysize = fontsize + "px"
  object.style.fontSize = mysize;
  // console.log(article);
  // console.log(getStyle(article, 'font-size'));
}

function mirrorText(object){
  // horizontal flip
  object.style.MozTransform = "scale(-1, 1)";
  object.style.WebkitTransform = "scale(-1, 1)";
  object.style.OTransform = "scale(-1, 1)";
  object.style.MsTransform = "scale(-1, 1)";
  object.style.transform = "scale(-1, 1)";
}

function resetText(object){
  // vertical flip
  object.style.MozTransform = "scale(1, 1)";
  object.style.WebkitTransform = "scale(1, 1)";
  object.style.OTransform = "scale(1, 1)";
  object.style.MsTransform = "scale(1, 1)";
  object.style.transform = "scale(1, 1)";
}

function changeTextStyle(){
  text = document.getElementById("text");
  if(text.style.mirror != "true"){
    mirrorText(text);
    text.style.mirror = "true";
  }
  else if(text.style.mirror == "true"){
    resetText(text);
    text.style.mirror = "false";
  }
}

function collapseMargin(){
  if(index < 500){
    index = index + 5;
    article = document.getElementsByTagName("article")[0];
    article.style.marginLeft = index+"px";
    article.style.marginRight = index+"px";
    console.log(index)
  }
}


function expandMargin(){
  if(index > 0){
    index = index - 5;
    article = document.getElementsByTagName("article")[0];
    article.style.marginLeft = index+"px";
    article.style.marginRight = index+"px";
    console.log(index)
  }
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}

addLoadEvent(init)